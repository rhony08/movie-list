package id.rhony.made_course.utils;

import android.net.Uri;

import id.rhony.made_course.BuildConfig;

/**
 * Created by Chevalier on 1/19/2019.
 */

public class MovieAPI {
    public String searchMovies(String key){
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("3")
                .appendPath("search")
                .appendPath("movie")
                .appendQueryParameter("api_key", BuildConfig.API_KEY)
                .appendQueryParameter("language","en-US")
                .appendQueryParameter("query", key)
                .build()
                .toString();
    }

    public String upcomingMovies(){
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("3")
                .appendPath("movie")
                .appendPath("upcoming")
                .appendQueryParameter("api_key", BuildConfig.API_KEY)
                .appendQueryParameter("language","en-US")
                .build()
                .toString();
    }

    public String nowPlayingMovies(){
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("3")
                .appendPath("movie")
                .appendPath("now_playing")
                .appendQueryParameter("api_key", BuildConfig.API_KEY)
                .appendQueryParameter("language","en-US")
                .build()
                .toString();
    }

    public String detailMovie(int id){
        return Uri.parse(BuildConfig.BASE_URL).buildUpon()
                .appendPath("3")
                .appendPath("movie")
                .appendPath(String.valueOf(id))
                .appendQueryParameter("api_key", BuildConfig.API_KEY)
                .appendQueryParameter("language","en-US")
                .build()
                .toString();
    }
}
