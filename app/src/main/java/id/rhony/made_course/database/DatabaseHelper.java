package id.rhony.made_course.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.OVERVIEW;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.POSTER;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TITLE;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns._ID;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TABLE_FAVORITE;

class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = TABLE_FAVORITE;

    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_FAVORITE = String.format("CREATE TABLE %s" +
            " (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL," +
            " %s TEXT NOT NULL)",
            TABLE_FAVORITE,
            _ID,
            TITLE,
            POSTER,
            OVERVIEW);

    DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVORITE);
        onCreate(sqLiteDatabase);
    }
}
