package id.rhony.made_course.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TABLE_FAVORITE;

public class FavoriteHelper {
    private static final String DATABASE_TABLE = TABLE_FAVORITE;
    private final DatabaseHelper mDbHelper;
    private static FavoriteHelper INSTANCE;

    private SQLiteDatabase mDb;

    private FavoriteHelper(Context context) {
        this.mDbHelper = new DatabaseHelper(context);
    }

    public static FavoriteHelper getInstance(Context context) {
        if (INSTANCE == null){
            synchronized (SQLiteOpenHelper.class) {
                INSTANCE = new FavoriteHelper(context);
            }
        }
        return INSTANCE;
    }

    public void open() throws SQLException {
        mDb = mDbHelper.getWritableDatabase();
    }

    /**
     * Get list of favorite movie
     * @return Cursor - which have all of movies
     */
    public Cursor queryProvider() {
        return mDb.query(
                DATABASE_TABLE,
                null,
                null,
                null,
                null,
                null,
                _ID + " DESC",
                null);
    }

    /**
     * Find Movie by Id
     * @param find - id of movie which want to find
     * @return Cursor - which have value of selected movie
     */
    public Cursor queryProvider(String find) {
        return mDb.query(
                DATABASE_TABLE,
                null,
                _ID + " = ?",
                new String[]{find},
                null,
                null,
                _ID + " DESC",
                null);
    }

    // TODO: insert to table
    /**
     * Add movie to favorite
     * @param values - ContentValues which was put all of movie data
     * @return long - id of movie from db
     */
    public long insert(ContentValues values) {
        return mDb.insert(DATABASE_TABLE, null, values);
    }

    // TODO: delete from table

    /**
     * Delete movie from favorite
     * @param id - id of movie which want to remove
     * @return int - count of movie which delete
     */
    public int delete(int id) {
        return mDb.delete(DATABASE_TABLE, _ID + " = ?", new String[]{String.valueOf(id)});
    }
}
