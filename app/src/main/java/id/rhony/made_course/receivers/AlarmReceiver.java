package id.rhony.made_course.receivers;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import id.rhony.made_course.R;
import id.rhony.made_course.models.Movie;
import id.rhony.made_course.models.MovieResponse;
import id.rhony.made_course.utils.MovieAPI;

public class AlarmReceiver extends BroadcastReceiver {
    public static final String TYPE_ONE_TIME = "Daily Alarm";
    public static final String TYPE_REPEATING = "Release Alarm";
    public static final String EXTRA_TYPE = "type";
    private final static String GROUP_KEY_NOTIFS = "group_key_notif";

    private final static int NOTIF_REQUEST_CODE = 200;
    private final int ID_DAILY = 100;
    private final int ID_RELEASE = 101;

    private int idNotif = 0;
    private ArrayList<Movie> mMovies;

    public AlarmReceiver() {
        mMovies = new ArrayList<>();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);

        boolean isDaily = type.equalsIgnoreCase(TYPE_ONE_TIME);
        String message = isDaily ? "Catalogue movie missing you" : "Movie release";
        String title = isDaily ? TYPE_ONE_TIME : TYPE_REPEATING;
        int notifId = isDaily ? ID_DAILY : ID_RELEASE;

        showToast(context, title, message);

        if (isDaily) {
            showDailyAlarmNotification(context, notifId);
        }else {
            idNotif = 0;
            showReleaseAlarmNotification(context, title, message, notifId);
        }
    }

    private void showToast(Context context, String title, String message) {
        Toast.makeText(context, title + " : " + message, Toast.LENGTH_LONG).show();
    }

    private void showReleaseAlarmNotification(Context context, String title, String message, int notifId) {
        String CHANNEL_ID = "Channel_Movie";
        String CHANNEL_NAME = "AlarmManager_Channel";
        String DATE_FORMAT = "yyyy-MM-dd";
        NotificationCompat.Builder builder;

        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        String currDate = dateFormat.format(new Date());

        mMovies = new ArrayList<>();
        ArrayList<Movie> currRelease = new ArrayList<>();
        getMovies(context);

        for(Movie movie: mMovies) {
            if (movie.getReleaseDate().equals(currDate)){
                currRelease.add(movie);
                idNotif++;
            }
        }
        mMovies.clear();

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Resources resources = context.getResources();

        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                NOTIF_REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        int maxNotif = 2;
        if (currRelease.size() == 0) return;
        else if (currRelease.size() < maxNotif) {
            Movie movie = currRelease.get(0);
            builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_favorite_full)
                    .setContentTitle(resources.getString(R.string.reminder_title))
                    .setContentText(resources.getString(R.string.release_title, movie.getMovieTitle()))
                    .setGroup(GROUP_KEY_NOTIFS)
                    .setAutoCancel(true)
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setContentIntent(pendingIntent)
                    .setSound(alarmSound);
        } else {
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle()
                    .setBigContentTitle(resources.getString(R.string.count_release, idNotif))
                    .setSummaryText(resources.getString(R.string.reminder_title));
            for (Movie movie: currRelease) {
                inboxStyle.addLine(resources.getString(R.string.release_title, movie.getMovieTitle()));
            }
            builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_favorite_full)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setGroup(GROUP_KEY_NOTIFS)
                    .setContentIntent(pendingIntent)
                    .setGroupSummary(true)
                    .setStyle(inboxStyle)
                    .setAutoCancel(true)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(CHANNEL_ID);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, builder.build());
        }

    }

    private void showDailyAlarmNotification(Context context, int notifId) {
        String CHANNEL_ID = "Channel_Movie_Daily";
        String CHANNEL_NAME = "AlarmManager_Channel_Daily";

        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Resources resources = context.getResources();

        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                NOTIF_REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_favorite_full)
                .setContentTitle(resources.getString(R.string.reminder_title))
                .setContentText(resources.getString(R.string.daily_reminder))
                .setGroup(GROUP_KEY_NOTIFS)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setContentIntent(pendingIntent)
                .setSound(alarmSound);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);

            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(CHANNEL_ID);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        if (notificationManagerCompat != null) {
            notificationManagerCompat.notify(notifId, builder.build());
        }

    }

    public void setRepeatingAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_TYPE, type);

        boolean isDaily = type.equals(TYPE_ONE_TIME);
        String timeArray[] = (isDaily) ? new String[]{"7","0"} : new String[]{"8","0"};

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);

        int requestCode = type.equalsIgnoreCase(TYPE_ONE_TIME) ? ID_DAILY : ID_RELEASE;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        Toast.makeText(context, "Repeating alarm set up", Toast.LENGTH_SHORT).show();
    }

    public void cancelAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        int requestCode = type.equalsIgnoreCase(TYPE_ONE_TIME) ? ID_DAILY : ID_RELEASE;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        pendingIntent.cancel();

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        Toast.makeText(context, "Repeating alarm dibatalkan", Toast.LENGTH_SHORT).show();
    }

    private void getMovies(Context context){
        try{
            RequestQueue queue = Volley.newRequestQueue(context);

            String url = new MovieAPI().nowPlayingMovies();
            Log.d("default-URL", url);

            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("default-Response", response);
                            Gson gson = new Gson();
                            MovieResponse movieResponse = gson.fromJson(response, MovieResponse.class);

                            mMovies.clear();
                            mMovies.addAll(movieResponse.getResults());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //
                }
            }
            );

            queue.add(stringRequest);
        }catch (Exception e){
            System.out.println("Catching error: " + e.getMessage());
        }
    }

}
