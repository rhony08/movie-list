package id.rhony.made_course.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Chevalier on 1/15/2019.
 */

public class Movie implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String movieTitle;
    @SerializedName("vote_average")
    private double vote;
    @SerializedName("original_language")
    private String lang;
    @SerializedName("release_date")
    private String date;
    @SerializedName("poster_path")
    private String poster;
    @SerializedName("backdrop_path")
    private String backdrop;
    @SerializedName("tagline")
    private String tagline;
    @SerializedName("overview")
    private String overview;
    @SerializedName("genres")
    private List<Genre> genres;

    public Movie() { }

    public Movie(int id, String movieTitle, double vote, String lang, String date, String poster, String backdrop, String tagline, String overview, List<Genre> genres) {
        this.id = id;
        this.movieTitle = movieTitle;
        this.vote = vote;
        this.lang = lang;
        this.date = date;
        this.poster = poster;
        this.tagline = tagline;
        this.overview = overview;
        this.genres = genres;
        this.backdrop = backdrop;

    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public double getVote() {
        return vote;
    }
    public String getLang() {
        return lang;
    }
    public String getReleaseDate() {
        return date;
    }
    public String getPoster() {
        return "https://image.tmdb.org/t/p/w185" + poster;
    }
    public String getPosterPath() {
        return poster;
    }
    public String getBackdrop() {
        return "https://image.tmdb.org/t/p/w185" + backdrop;
    }
    public String getBackdropPath() {
        return backdrop;
    }
    public int getId() {
        return id;
    }
    public String getOverview() {
        return overview;
    }
    public List<Genre> getGenres() {
        return genres;
    }
    public String getTagline() {
        return tagline;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
