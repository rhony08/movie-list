package id.rhony.made_course.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chevalier on 1/19/2019.
 */

public class Genre {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    public Genre(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
