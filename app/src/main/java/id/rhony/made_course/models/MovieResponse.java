package id.rhony.made_course.models;

import java.util.List;

/**
 * Created by Chevalier on 1/19/2019.
 */

public class MovieResponse {
    private List<Movie> results;

    public MovieResponse(List<Movie> results) {
        this.results = results;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
