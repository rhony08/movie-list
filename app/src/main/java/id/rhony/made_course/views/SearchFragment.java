package id.rhony.made_course.views;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;

import id.rhony.made_course.R;
import id.rhony.made_course.adapters.MovieAdapter;
import id.rhony.made_course.models.Movie;
import id.rhony.made_course.models.MovieResponse;
import id.rhony.made_course.utils.MovieAPI;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements MovieAdapter.Handler {
    private Context mContext;
    private EditText mSearchBox;
    private ArrayList<Movie> mMovies;
    private MovieAdapter mAdapter;
    private RequestQueue mQueue;

    public SearchFragment() {
        mMovies = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        mContext = view.getContext();
        Button mSearch = view.findViewById(R.id.btn_search);
        mSearchBox = view.findViewById(R.id.et_search_bar);
        RecyclerView recyclerView = view.findViewById(R.id.rView);
        mQueue = Volley.newRequestQueue(mContext);

        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String query = mSearchBox.getText().toString();
                if (!TextUtils.isEmpty(query)) {
                    getMovies(query);
                }else {
                    Toast.makeText(view.getContext(), "Query is required",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(
                recyclerView.getContext(), linearLayoutManager.getOrientation()
        ));

        mAdapter = new MovieAdapter(mMovies, this);
        recyclerView.setAdapter(mAdapter);

        getMovies("Avenger");
        return view;
    }

    private void getMovies(String query){
        try{
            String url = new MovieAPI().searchMovies(query);
            Log.d("search-URL", url);

            StringRequest stringRequest = new StringRequest(
                Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("search-Response", response);
                        Gson gson = new Gson();
                        MovieResponse movieResponse = gson.fromJson(response, MovieResponse.class);

                        mMovies.clear();
                        mMovies.addAll(movieResponse.getResults());
                        mAdapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //
                    }
                }
            );

            mQueue.add(stringRequest);
        }catch (Exception e){
            System.out.println("Catching error: " + e.getMessage());
        }
    }

    @Override
    public void clickedObject(Movie movie) {
        Intent intent = new Intent(mContext, DetailActivity.class);
        intent.putExtra("movie", movie.getId());
        mContext.startActivity(intent);
    }
}
