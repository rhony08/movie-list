package id.rhony.made_course.views;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.PreferenceFragmentCompat;

import id.rhony.made_course.R;
import id.rhony.made_course.receivers.AlarmReceiver;

public class MyPreferenceFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    private String DAILY_REMINDER;
    private String RELEASE_REMINDER;
    private CheckBoxPreference pref_daily;
    private CheckBoxPreference pref_release;
    private AlarmReceiver alarmReceiver;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
        init();
    }

    private void init() {
        DAILY_REMINDER = getResources().getString(R.string.key_daily_reminder);
        RELEASE_REMINDER = getResources().getString(R.string.key_release_reminder);

        pref_daily = (CheckBoxPreference) findPreference(DAILY_REMINDER);
        pref_release = (CheckBoxPreference) findPreference(RELEASE_REMINDER);
        alarmReceiver = new AlarmReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(DAILY_REMINDER)) {
            boolean status = sharedPreferences.getBoolean(DAILY_REMINDER, true);
            pref_daily.setChecked(status);
            if (status)
                alarmReceiver.setRepeatingAlarm(getContext(), AlarmReceiver.TYPE_ONE_TIME);
            else
                alarmReceiver.cancelAlarm(getContext(), AlarmReceiver.TYPE_ONE_TIME);
        }

        if (s.equals(RELEASE_REMINDER)) {
            boolean status = sharedPreferences.getBoolean(RELEASE_REMINDER, true);
            pref_release.setChecked(status);
            if (status)
                alarmReceiver.setRepeatingAlarm(getContext(), AlarmReceiver.TYPE_REPEATING);
            else
                alarmReceiver.cancelAlarm(getContext(), AlarmReceiver.TYPE_REPEATING);
        }
    }
}
