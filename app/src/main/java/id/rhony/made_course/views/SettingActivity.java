package id.rhony.made_course.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.rhony.made_course.R;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.setting_frame_layout, new MyPreferenceFragment())
                .commit();
    }
}
