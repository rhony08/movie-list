package id.rhony.made_course.views;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.ArrayList;

import id.rhony.made_course.R;
import id.rhony.made_course.adapters.MovieAdapter;
import id.rhony.made_course.models.Movie;
import id.rhony.made_course.models.MovieResponse;
import id.rhony.made_course.utils.MovieAPI;

import static android.provider.BaseColumns._ID;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.OVERVIEW;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.POSTER;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TITLE;

/**
 * A simple {@link Fragment} subclass.
 */
public class DefaultFragment extends Fragment implements
        MovieAdapter.Handler, LoaderManager.LoaderCallbacks<Cursor> {
    private Context mContext;
    private int mType = 0;
    private ArrayList<Movie> mMovies;
    private MovieAdapter mAdapter;
    private RequestQueue mQueue;
    private final int LOAD_ID = 101;
    private final String KEY_DATA_LIST = "data-list";
    private final String KEY_DATA_TYPE = "type";
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public DefaultFragment() {
        // Required empty public constructor
        mMovies = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_default, container, false);
        mContext = view.getContext();

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_fragment);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMovies();
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.rView);
        mQueue = Volley.newRequestQueue(mContext);
        mType = getArguments().getInt(KEY_DATA_TYPE, -1);
        if (mType == -1) {
            mType = savedInstanceState.getInt(KEY_DATA_TYPE, 0);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(
                recyclerView.getContext(), linearLayoutManager.getOrientation()
        ));

        mAdapter = new MovieAdapter(mMovies, this);
        recyclerView.setAdapter(mAdapter);

        if (savedInstanceState == null) {
            loadMovies();
        } else {
            mMovies.addAll((ArrayList<Movie>) savedInstanceState.getSerializable(KEY_DATA_LIST));
            mAdapter.notifyDataSetChanged();
        }

        return view;
    }

    private void loadMovies(){
        mSwipeRefreshLayout.setRefreshing(true);
        if (mType != 2) getMovies();
        else {
            getLoaderManager().initLoader(LOAD_ID, null, this);
        }
    }

    private void setState() {
        Bundle bundle = new Bundle();
        onSaveInstanceState(bundle);
    }

    private void getMovies(){
        try{
            String url = (mType == 0) ? new MovieAPI().nowPlayingMovies() : new MovieAPI().upcomingMovies();
            Log.d("default-URL", url);

            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("default-Response", response);
                            Gson gson = new Gson();
                            MovieResponse movieResponse = gson.fromJson(response, MovieResponse.class);

                            mMovies.clear();
                            mMovies.addAll(movieResponse.getResults());
                            setState();
                            mAdapter.notifyDataSetChanged();
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //
                }
            }
            );

            mQueue.add(stringRequest);
        }catch (Exception e){
            System.out.println("Catching error: " + e.getMessage());
        }
    }

    @Override
    public void clickedObject(Movie movie) {
        Intent intent = new Intent(mContext, DetailActivity.class);
        intent.putExtra("movie", movie.getId());
        intent.putExtra("movie_name", movie.getMovieTitle());
        mContext.startActivity(intent);
    }

    // TODO: get all of favorite's movies
    /**
     * Get all of favorite's movies
     * @return ArrayList - list of movie
     */
    public ArrayList<Movie> getMovies(Cursor cursor) {
        ArrayList<Movie> movieArrayList = new ArrayList<>();
        cursor.moveToFirst();

        if (cursor.getCount() > 0){
            Movie movie;
            do {
                movie = new Movie();
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                movie.setMovieTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                movie.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));

                movieArrayList.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        } else {
            Toast.makeText(getContext(), "No data available", Toast.LENGTH_SHORT).show();
        }
        return movieArrayList;
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new CursorLoader(getContext(), CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        mMovies.addAll(getMovies(data));
        setState();
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        mMovies.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mType == 2) {
            mMovies.clear();
            mAdapter.notifyDataSetChanged();
            getLoaderManager().restartLoader(LOAD_ID, null, this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mType == 2) getLoaderManager().destroyLoader(LOAD_ID);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(KEY_DATA_LIST, mMovies);
        outState.putSerializable(KEY_DATA_TYPE, mType);
        super.onSaveInstanceState(outState);
    }
}
