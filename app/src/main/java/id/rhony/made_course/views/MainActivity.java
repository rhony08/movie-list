package id.rhony.made_course.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import id.rhony.made_course.R;
import id.rhony.made_course.receivers.AlarmReceiver;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {
    final String TYPE = "type";
    final int NOW = 0;
    final int UPCOMING = 1;
    final int FAV = 2;
    final int SEARCH = 3;
    private int mSelectedMenu;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView btmNav = findViewById(R.id.main_btm_nav);
        btmNav.setOnNavigationItemSelectedListener(this);
        btmNav.setSelectedItemId(R.id.btm_nav_now_playing);

        Bundle bundle = new Bundle();
        if (savedInstanceState != null) {
            bundle = savedInstanceState;
            mSelectedMenu = savedInstanceState.getInt(TYPE);
            fragment = (mSelectedMenu < 3) ? new DefaultFragment() : new SearchFragment();
        } else {
            mSelectedMenu = NOW;
            fragment = new DefaultFragment();
            bundle.putInt(TYPE, NOW);
        }
        fragment.setArguments(bundle);
        loadFragment(fragment);
        isFirstRun();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.lang_setting:
                startActivity(new Intent(Settings.ACTION_LOCALE_SETTINGS));
                return true;
            case R.id.reminder_setting:
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_frame_layout, fragment)
                    .commitAllowingStateLoss();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()){
            case R.id.btm_nav_now_playing:
                fragment = new DefaultFragment();
                mSelectedMenu = NOW;
                break;
            case R.id.btm_nav_up_coming:
                fragment = new DefaultFragment();
                mSelectedMenu = UPCOMING;
                break;
            case R.id.btm_nav_fav:
                fragment = new DefaultFragment();
                mSelectedMenu = FAV;
                break;
            case R.id.btm_nav_search:
                fragment = new SearchFragment();
                mSelectedMenu = SEARCH;
                break;
        }
        bundle.putInt(TYPE, mSelectedMenu);
        fragment.setArguments(bundle);
        onSaveInstanceState(bundle);
        return loadFragment(fragment);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TYPE, mSelectedMenu);
    }

    private void isFirstRun(){
        String key = getResources().getString(R.string.is_first_run);
        String pref_key = "movie_list_key";
        SharedPreferences preferences = getSharedPreferences(pref_key, MODE_PRIVATE);
        boolean isFirstRun = preferences.getBoolean(key, true);
        if (isFirstRun) {
            AlarmReceiver receiver = new AlarmReceiver();
            receiver.setRepeatingAlarm(this, AlarmReceiver.TYPE_ONE_TIME);
            receiver.setRepeatingAlarm(this, AlarmReceiver.TYPE_REPEATING);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(key, false);
            editor.apply();
        }
    }
}
