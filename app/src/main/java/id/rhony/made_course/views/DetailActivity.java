package id.rhony.made_course.views;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.rhony.made_course.R;
import id.rhony.made_course.adapters.GenreAdapter;
import id.rhony.made_course.models.Genre;
import id.rhony.made_course.models.Movie;
import id.rhony.made_course.utils.MovieAPI;
import id.rhony.made_course.widgets.FavoriteWidget;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

import static android.provider.BaseColumns._ID;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.OVERVIEW;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.POSTER;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TITLE;

public class DetailActivity extends AppCompatActivity {
    private int mId;
    private Movie mMovie;
    private List<Genre> mGenres;
    private GenreAdapter mAdaper;
    private RequestQueue mQueue;
    private ImageView mImageView;
    private TextView mTitle, mTagline, mRating, mLang, mRelease, mOverview;
    private boolean isFavorite;
    private Uri mUri;
    private MenuItem mFavMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mId = getIntent().getIntExtra("movie", 0);
        mUri = Uri.parse(CONTENT_URI + "/" + mId);

        mGenres = new ArrayList<>();
        mImageView = findViewById(R.id.detail_poster);
        mTitle = findViewById(R.id.detail_title);
        mTagline = findViewById(R.id.detail_tagline);
        mRating = findViewById(R.id.detail_rating);
        mLang = findViewById(R.id.detail_language);
        mRelease = findViewById(R.id.detail_release);
        mOverview = findViewById(R.id.detail_overview);

        RecyclerView mRview = findViewById(R.id.detail_genres);
        mAdaper = new GenreAdapter(mGenres);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        mRview.setLayoutManager(layoutManager);
        mRview.setAdapter(mAdaper);

        mQueue = Volley.newRequestQueue(this);

        getMovies();
    }

    private void isFavorite() {
        Cursor cursor = getContentResolver().query(mUri, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            isFavorite = true;
            cursor.close();
        }else {
            isFavorite = false;
        }
        Log.d(DetailActivity.class.getSimpleName(), "Before setState from isFavorite");
        if (mFavMenu != null) setFavorite();
    }

    private void setFavorite() {
        mFavMenu.setIcon(ContextCompat.getDrawable(this, (isFavorite) ? R.drawable.ic_favorite_full : R.drawable.ic_favorite_border));
    }

    private void updateWidget() {
        Intent intent = new Intent(this, FavoriteWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int ids[] = appWidgetManager
                .getAppWidgetIds(new ComponentName(this, FavoriteWidget.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        for (int id:
             ids) {
            Log.d(DetailActivity.class.getSimpleName(), String.valueOf(id));
        }
        sendBroadcast(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        mFavMenu = menu.findItem(R.id.menus_detail_fav);
        Log.d(DetailActivity.class.getSimpleName(), "Before setState");
        setFavorite();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menus_detail_fav:
                if (!isFavorite) {
                    Uri uri = getContentResolver().insert(CONTENT_URI, changeToContentValues(mMovie));
                    if (uri != CONTENT_URI){
                        Toast.makeText(this, "Added to Favorite",
                                Toast.LENGTH_SHORT).show();
                        isFavorite = true;
                    } else {
                        Toast.makeText(this, "Failed when add new favorite",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    int count = getContentResolver().delete(mUri, null, null);
                    if (count != 0){
                        Toast.makeText(this, "Removed from Favorite",
                                Toast.LENGTH_SHORT).show();
                        isFavorite = false;
                    } else {
                        Toast.makeText(this, "Failed when remove favorite",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                setFavorite();
                updateWidget();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // TODO: Put Movie to ContentValues
    /**
     * Add movie to favorite
     * @param movie - movie which will save to db
     * @return ContentValues - ContentValues which was put all of movie data
     */
    private ContentValues changeToContentValues(Movie movie) {
        ContentValues values = new ContentValues();
        values.put(_ID, movie.getId());
        values.put(TITLE, movie.getMovieTitle());
        values.put(OVERVIEW, movie.getOverview());
        values.put(POSTER, movie.getBackdropPath());
        return values;
    }

    private void setAllView(){
        Glide.with(this)
                .load(mMovie.getPoster())
                .apply(RequestOptions.bitmapTransform(new CropCircleTransformation()))
                .into(mImageView);
        mTitle.setText(mMovie.getMovieTitle());
        mTagline.setText(mMovie.getTagline());
        mRating.setText(String.valueOf(mMovie.getVote()));
        mLang.setText(String.valueOf(mMovie.getLang()));
        mRelease.setText(String.valueOf(mMovie.getReleaseDate()));
        mOverview.setText(String.valueOf(mMovie.getOverview()));

        mGenres.clear();
        mGenres.addAll(mMovie.getGenres());
        mAdaper.notifyDataSetChanged();
    }

    private void getMovies(){
        isFavorite();
        try{
            String url = new MovieAPI().detailMovie(mId);
            StringRequest stringRequest = new StringRequest(
                    Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("default-Response", response);
                            Gson gson = new Gson();
                            mMovie = gson.fromJson(response, Movie.class);
                            setAllView();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //
                }
            }
            );

            mQueue.add(stringRequest);
        }catch (Exception e){
            System.out.println("Catching error: " + e.getMessage());
        }
    }
}
