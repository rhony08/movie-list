package id.rhony.made_course.widgets;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import id.rhony.made_course.R;

import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.POSTER;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private List<String> mWidgetItems = new ArrayList<>();

    public StackRemoteViewsFactory(Context context) {
        this.mContext = context;
    }

    @Override
    public void onCreate() {
        getFavoriteMovies(mContext);
    }

    @Override
    public void onDataSetChanged() {
        getFavoriteMovies(mContext);
    }

    private void getFavoriteMovies(Context context) {
        mWidgetItems.clear();
        final long identityToken = Binder.clearCallingIdentity();
        Cursor cursor = context.getContentResolver().query(CONTENT_URI, new String[]{POSTER}, null, null, null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            do {
                String path = "https://image.tmdb.org/t/p/w185" + cursor.getString(cursor.getColumnIndexOrThrow(POSTER));
                mWidgetItems.add(path);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }
        Log.d(StackRemoteViewsFactory.class.getSimpleName(), String.valueOf(mWidgetItems.size()));
        Binder.restoreCallingIdentity(identityToken);
    }

    @Override
    public void onDestroy() {}

    @Override
    public int getCount() {
        return mWidgetItems.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        try {
            remoteViews.setImageViewBitmap(R.id.imageView,
                    Glide.with(mContext)
                            .asBitmap()
                            .load(mWidgetItems.get(i))
                            .apply(new RequestOptions().fitCenter())
                            .submit()
                            .get()
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putInt(FavoriteWidget.EXTRA_ITEM, i);
        Intent intent = new Intent();
        intent.putExtras(bundle);

        remoteViews.setOnClickFillInIntent(R.id.imageView, intent);
        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
