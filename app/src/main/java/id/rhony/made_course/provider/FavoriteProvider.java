package id.rhony.made_course.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import id.rhony.made_course.database.FavoriteHelper;

import static id.rhony.made_course.database.DatabaseContract.AUTHORITY;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rhony.made_course.database.DatabaseContract.FavoriteColumns.TABLE_FAVORITE;

public class FavoriteProvider extends ContentProvider {
    private static final int FAVORITE = 1;
    private static final int FAVORITE_ID = 2;

    public static final UriMatcher sUriMathcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        // content://id.rhony.made_course/favorite
        sUriMathcher.addURI(AUTHORITY, TABLE_FAVORITE , FAVORITE);

        // content://id.rhony.made_course/favorite/id
        sUriMathcher.addURI(AUTHORITY,
                TABLE_FAVORITE + "/#" ,
                FAVORITE_ID);
    }

    private FavoriteHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = FavoriteHelper.getInstance(getContext());
        mHelper.open();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Cursor cursor;
        switch (sUriMathcher.match(uri)) {
            case FAVORITE :
                cursor = mHelper.queryProvider();
                break;
            case FAVORITE_ID:
                cursor = mHelper.queryProvider(uri.getLastPathSegment());
                break;
            default:
                cursor = null;
                break;
        }

        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        long added;

        switch (sUriMathcher.match(uri)) {
            case FAVORITE :
                added = mHelper.insert(contentValues);
                break;
            default:
                added = 0;
                break;
        }

        if (added != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return Uri.parse(CONTENT_URI + "/" + added);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int deleted;

        switch (sUriMathcher.match(uri)) {
            case FAVORITE_ID :
                deleted = mHelper.delete(Integer.parseInt(uri.getLastPathSegment()));
                Log.d(FavoriteProvider.class.getSimpleName(), String.valueOf(deleted));
                break;
            default:
                deleted = 0;
                break;
        }

        if (deleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
