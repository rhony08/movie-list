package id.rhony.made_course.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import id.rhony.made_course.R;
import id.rhony.made_course.models.Genre;

/**
 * Created by Chevalier on 1/19/2019.
 */

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreHolder> {
    private List<Genre> mGenre;

    public GenreAdapter(List<Genre> mGenre) {
        this.mGenre = mGenre;
    }

    @NonNull
    @Override
    public GenreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.genre_list, parent, false);
        return new GenreHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreHolder holder, int position) {
        holder.bindItem(mGenre.get(position));
    }

    @Override
    public int getItemCount() {
        return mGenre.size();
    }

    class GenreHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public GenreHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_title);
        }

        public void bindItem(Genre genre){
            title.setText(String.valueOf(genre.getName()));
        }
    }
}
