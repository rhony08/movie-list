package id.rhony.favorite_movie.database;

import android.net.Uri;
import android.provider.BaseColumns;

public final class DatabaseContract {
    // Authority
    private static final String AUTHORITY = "id.rhony.made_course";
    private static final String SCHEME = "content";

    public DatabaseContract() { }

    public static final class FavoriteColumns implements BaseColumns {

        static final String TABLE_FAVORITE = "favorite";

        public static final String TITLE = "title";
        public static final String OVERVIEW = "overview";
        public static final String POSTER = "poster";

        // Base content untuk akses content provider
        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_FAVORITE)
                .build();

    }
}
