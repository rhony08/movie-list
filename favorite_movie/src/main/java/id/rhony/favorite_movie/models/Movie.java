package id.rhony.favorite_movie.models;

import java.io.Serializable;

/**
 * Created by Chevalier on 1/15/2019.
 */

public class Movie implements Serializable {
    private int id;
    private String movieTitle;
    private String poster;
    private String overview;

    public Movie() { }

    public String getMovieTitle() {
        return movieTitle;
    }
    public String getPoster() {
        return "https://image.tmdb.org/t/p/w185" + poster;
    }
    public int getId() {
        return id;
    }
    public String getOverview() {
        return overview;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
