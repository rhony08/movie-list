package id.rhony.favorite_movie.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import id.rhony.favorite_movie.models.Movie;
import id.rhony.favorite_movie.R;

/**
 * Created by Chevalier on 1/15/2019.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private List<Movie> mMovies;

    public MovieAdapter(List<Movie> movies) {
        this.mMovies = movies;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new MovieHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Movie currMovie = mMovies.get(position);
        holder.bindItem(currMovie);
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    class MovieHolder extends RecyclerView.ViewHolder {
        ImageView poster;
        TextView title;
        TextView overview;
        View itemView;

        MovieHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            poster = itemView.findViewById(R.id.img_poster);
            title = itemView.findViewById(R.id.tv_title);
            overview = itemView.findViewById(R.id.tv_overview);
        }

        void bindItem(Movie movie){
            title.setText(movie.getMovieTitle());
            overview.setText(movie.getOverview());
            Glide.with(itemView.getContext())
                    .load(movie.getPoster()).into(poster);
        }
    }
}
