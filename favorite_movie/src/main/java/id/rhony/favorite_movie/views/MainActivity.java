package id.rhony.favorite_movie.views;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.rhony.favorite_movie.R;
import id.rhony.favorite_movie.adapters.MovieAdapter;
import id.rhony.favorite_movie.models.Movie;

import static android.provider.BaseColumns._ID;
import static id.rhony.favorite_movie.database.DatabaseContract.FavoriteColumns.CONTENT_URI;
import static id.rhony.favorite_movie.database.DatabaseContract.FavoriteColumns.OVERVIEW;
import static id.rhony.favorite_movie.database.DatabaseContract.FavoriteColumns.POSTER;
import static id.rhony.favorite_movie.database.DatabaseContract.FavoriteColumns.TITLE;

public class MainActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<Cursor> {
    private List<Movie> mMovies;
    private MovieAdapter mAdapter;
    private final int LOAD_ID = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMovies = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(
                recyclerView.getContext(), linearLayoutManager.getOrientation()
        ));

        mAdapter = new MovieAdapter(mMovies);
        recyclerView.setAdapter(mAdapter);

        getLoaderManager().initLoader(LOAD_ID, null, this);
    }

    // TODO: get all of favorite's movies
    /**
     * Get all of favorite's movies
     * @return ArrayList - list of movie
     */
    public ArrayList<Movie> getMovies(Cursor cursor) {
        ArrayList<Movie> movieArrayList = new ArrayList<>();
        cursor.moveToFirst();

        if (cursor.getCount() > 0){
            Movie movie;
            do {
                movie = new Movie();
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                movie.setMovieTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                movie.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));

                movieArrayList.add(movie);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        } else {
            Toast.makeText(this, "No data available", Toast.LENGTH_SHORT).show();
        }
        return movieArrayList;
    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new CursorLoader(this, CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
        mMovies.addAll(getMovies(cursor));
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        mMovies.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(LOAD_ID);
    }
}
